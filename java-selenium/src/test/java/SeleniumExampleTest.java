import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

public class SeleniumExampleTest {
 
    /*
    private static SeleniumExample seleniumExample;
 
    @BeforeClass
    public static void setUp() {
        seleniumExample = new SeleniumExample();
    }

    @AfterClass
    public static void tearDown() {
        seleniumExample.closeWindow();
    }

    @Test
    public void correctTitle() {
        String actualTitle = seleniumExample.getTitle();
        String expectedValue = "ACL - Track Your Cargo";

        assertThat(actualTitle, is(expectedValue));
    }
    */

    @Test
    public void correctTitle() {
        SeleniumExample example = new SeleniumExample();
        String actualTitle = example.getTitle();
        String expectedValue = "Google";

        assertThat(actualTitle, is(expectedValue));
    }

    @Test
    public void falseTitle() {
        SeleniumExample example = new SeleniumExample();
        String actualTitle = example.getTitle();
        String expectedValue = "Googles";

        assertThat(actualTitle, is(expectedValue));
    }
}
