public class SeleniumExample {
 
    private SeleniumConfig config;
    //private String url = "http://www.aclcargo.com/trackCargo.php/";
    private String url = "https://www.google.de/";
 
    public SeleniumExample() {
        config = new SeleniumConfig();
        config.getDriver().get(url);
    }

    public String getTitle() {
        return this.config.getDriver().getTitle();
    }

    public void closeWindow() {
        this.config.getDriver().close();
    }
}
