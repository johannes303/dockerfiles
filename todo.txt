1. JVM Docker
=============

-XX:+UnlockExperimentalVMOptions
-XX:+UseCGroupMemoryLimitForHeap

JVM is not cgroups aware before JDK 1.8_191

$ docker run openjdk:8-jdk java -XX:+PrintFlagsFinal -version | grep MaxHeapSize

$ docker run -m 512MB openjdk:8-jdk java -XX:+PrintFlagsFinal -version | grep MaxHeapSize
docker run -m 512MB (sets cgroups constraints)

$ docker run -m 512MB openjdk:8-jdk java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap  -XX:+PrintFlagsFinal -version | grep MaxHeapSize


Multistage Build
================


Docker Compose Postgres
=======================


Serialize Data from ACL
=======================


Mockito
=======


jib-maven-plugin
================
can be run without modifying pom.xml testwise

$ mvn verify -Pdev,skipTestsAndYarn com.google.cloud.tools:jib-maven-plugin:0.9.11:dockerBuild




