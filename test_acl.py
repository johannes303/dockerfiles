from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

def get_driver():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(options=chrome_options)
    driver.implicitly_wait(15)
    return driver

def test_google_title():
    driver = get_driver()
    driver.get('https://www.google.de')
    elem = driver.title
    assert elem == 'Google'

#def test_acl():
#    chrome_options = webdriver.ChromeOptions()
#    chrome_options.add_argument('--no-sandbox')
#    chrome_options.add_argument('--headless')
#    chrome_options.add_argument('--disable-gpu')
#    driver = webdriver.Chrome(options=chrome_options)
#    driver.implicitly_wait(10)
    #driver.get('http://www.aclcargo.com/trackCargo.php')
    #elem = driver.find_element_by_xpath('/html/body/div/div/a')
    #assert elem.is_displayed()
