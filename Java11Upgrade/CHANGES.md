Following changes were made
===========================

The directory *driver* is not needed at the moment. At least when using Chromium
and chromium driver which ship with the Debian Docker distro.


application-dev.properties
--------------------------
1. In here one has to change the Postgres ip address.


pom.xml
-------
In here the modules *FmsCarrierMaersk* and *FmsCarrierMsc* were removed for
only testing carrier Acl. Then it is necessary to add the upstream Spring Boot
repository to get the latest version of Spring Boot which has Java11 support.

~~~ xml
<repositories>
  <repository>
    <id>repository.spring.milestone</id>
    <name>Spring Milestone Repository</name>
    <url>http://repo.spring.io/milestone</url>
  </repository>
</repositories>
<pluginRepositories>
  <pluginRepository>
    <id>repository.spring.milestone</id>
    <name>Spring Milestone Repository</name>
    <url>http://repo.spring.io/milestone</url>
  </pluginRepository>
</pluginRepositories>
~~~


Java11Upgrade/FmsCarrierCommons/src/main/java/com/fms/carrier/util/AppUtils.java
--------------------------------------------------------------------------------
1. In this class the if clause in *getDriverInstance* method got removed. I think
every class should be independent on environment things. These things should
be injected from outside.

2. I had to add
   *singletonDriverInstance.manage().timeouts().implicitlyWait(30,
   TimeUnit.SECONDS);* to the method as well to make it work. 30 seconds maybe
   to high but it works on my side.

3. For Java11 in at least in the moment I didn't get chromedriver to work
   without the option *--no-sandbox*. With the Java8 version I got it to work
   without the sandbox being disabled. I can push that also to a repository if wanted.
   And I will look into it to get it to work also in Java11 with sandbox enabled.



