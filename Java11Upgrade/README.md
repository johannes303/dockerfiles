Java11
======


Build the docker image
----------------------
docker build -t debian-jdk11-base -f Dockerfile.debian-jdk11-base .


Start the docker image
----------------------
The docker image is in the shell mode for testing purposes (CMD ["sh"]). You
can start it with:

docker run -it debian-jdk11-base


Run the Java Scraper
--------------------
Inside the container use following command to start scraping.

java -jar FmsCarrierAcl/target/FmsCarrierAcl.jar --spring.config.additional-location=./properties/ --spring.profiles.active=dev



