CREATE TABLE IF NOT EXISTS webtracking_input (
		id SERIAL PRIMARY KEY,
		carrier VARCHAR(3),
		booking_ref VARCHAR(35),
		bl_number VARCHAR(35),
		containerno TEXT,
		processing_timestamp TIMESTAMP,
		status VARCHAR(100),
		mindate date DEFAULT NULL
	);
	
CREATE TABLE IF NOT EXISTS webtracking_output (
		id SERIAL PRIMARY KEY,
		carrier VARCHAR(3),
		booking_ref VARCHAR(35),
		bl_number VARCHAR(35),
		containerno TEXT,
  		status VARCHAR(100),
  		status_date timestamp without time zone,
  		status_location VARCHAR(200),
  		status_vessel VARCHAR(100),
  		status_voyage VARCHAR(50),
  		processed_timestamp TIMESTAMP default CURRENT_TIMESTAMP
	);
