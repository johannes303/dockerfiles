package com.fms.carrier.dao.mapper;

import java.sql.Timestamp;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.fms.carrier.dto.ResultTableDTO;
import com.fms.carrier.dto.SourceTableDTO;
import com.fms.carrier.endpoints.ScrapingJobMetricsEndpoint.Carrier;

@Mapper
public interface DataMapper {

	public static String QRY_TO_GET_INPUT_INCLUDING_ERRORS = "select * from webtracking_input where carrier = #{carrier} and (status is null or status not in('ok','error'))";

	public static String QRY_TO_GET_INPUT_UNPROCESSED = "select * from webtracking_input where carrier = #{carrier} and status is null";

	public static String QRY_TO_SAVE_OUPUT = "insert into webtracking_output(carrier, booking_ref, bl_number, containerno, status, status_date, "
			+ "status_location, status_vessel, status_voyage) values(#{carrier}, #{bookingRef}, #{blNumber}, #{containerno}, "
			+ "#{status}, #{statusDate}, #{statusLocation}, #{statusVessel}, #{statusVoyage})";

	public static String QRY_TO_UPDATE_INPUT_PROCESSINGTIME_AND_STATUS = "update webtracking_input set processing_timestamp = #{processingTimestamp}, status = #{status} "
			+ "where carrier=#{carrier} and booking_ref = #{bookingRef} and bl_number=#{blNumber} and containerno = #{containerno}";

	public static String QRY_TO_GET_COUNT_OF_JOBS = "select carrier,status,count(carrier) from webtracking_input group by carrier,status order by carrier desc";
	
	public static String QRY_TO_GET_COUNT_OF_JOBS_BY_STATUS = "select carrier,count(carrier) as count from webtracking_input where status = #{status} group by carrier";
	
	public static String QRY_TO_GET_COUNT_OF_JOB_BY_CARRIER_ND_STATUS = "select carrier,count(carrier) as count from webtracking_input where status = #{status} and carrier=#{carrier} group by carrier";
	
	@Select(QRY_TO_GET_INPUT_UNPROCESSED)
	@Results({ @Result(property = "bookingRef", column = "booking_ref"),
			@Result(property = "blNumber", column = "bl_number"),
			@Result(property = "containerno", column = "containerno"),
			@Result(property = "carrier", column = "carrier"), })
	public List<SourceTableDTO> getInputDataset(@Param("carrier") String carrier);

	@Insert(QRY_TO_SAVE_OUPUT)
	public int saveOutputDataSet(ResultTableDTO resultTableDTO);

	@Update(QRY_TO_UPDATE_INPUT_PROCESSINGTIME_AND_STATUS)
	public int updateInputDataSetProcessingTimeAndStatus(
			@Param("carrier") String carrier,
			@Param("containerno") String containerno,
			@Param("bookingRef") String bookingRef,
			@Param("blNumber") String blNumber, @Param("status") String status,
			@Param("processingTimestamp") Timestamp processingTimestamp);
	
	@Select(QRY_TO_GET_COUNT_OF_JOBS_BY_STATUS)
	@Results({ @Result(property = "count", column = "count"), @Result(property = "name", column = "carrier") })
	public List<Carrier> getCountOfAllJobsByStatus(@Param("status") String Status);
	
	@Select(QRY_TO_GET_COUNT_OF_JOB_BY_CARRIER_ND_STATUS)
	@Results({ @Result(property = "count", column = "count"), @Result(property = "name", column = "carrier") })
	public List<Carrier> getCountOfJobByStatus(@Param("carrier") String carrier, @Param("status") String Status);
	
	@Select(QRY_TO_GET_COUNT_OF_JOBS)
	@Results({ @Result(property = "count", column = "count"), @Result(property = "name", column = "carrier") })
	public List<Carrier> getCountOfJobs();
}