package com.fms.carrier.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fms.carrier.dao.CarrierDAO;
import com.fms.carrier.dao.mapper.DataMapper;
import com.fms.carrier.dto.ResultTableDTO;
import com.fms.carrier.dto.SourceTableDTO;
import com.fms.carrier.endpoints.ScrapingJobMetricsEndpoint.Carrier;

@Repository
public class CarrierDAOImpl implements CarrierDAO {

	@Autowired
	private DataMapper dataMapper;
	
	public List<SourceTableDTO> getInputDataset(String carrier) {
		return dataMapper.getInputDataset(carrier);
	}

	public int saveContainerStatus(ResultTableDTO resultTableDTO) {
		return dataMapper.saveOutputDataSet(resultTableDTO);
	}

	public int updateInputProcessingTimeAndStatus(String carrier, String containerno, String bookingRef, 
			String blNumber, String status, Timestamp processingTimestamp) {
		return dataMapper.updateInputDataSetProcessingTimeAndStatus(carrier, containerno, bookingRef, blNumber, status, processingTimestamp);
	}
	
	public List<Carrier>  getCountOfJobs(String carrier, String status) {
		if (status != null && carrier != null) {
			return dataMapper.getCountOfJobByStatus(carrier,status);
		} else if(status != null){
			return dataMapper.getCountOfAllJobsByStatus(status);
		} else {
			return dataMapper.getCountOfJobs();
		}
	}
}
