package com.fms.carrier.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fms.carrier.dto.ResultTableDTO;
import com.fms.carrier.dto.SourceTableDTO;

public class ParseUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ParseUtil.class);

	public static List<ResultTableDTO> parseScrappedTableElementBySelectorQry(Document page,String tbodySelectorElemId, String trSelectorElemId, String tdSelectorElemId,
			SourceTableDTO sourceTableDTO, Map<String, String> configMap, String containerNo,String dateFormat,String timestampFormat) {
		List<ResultTableDTO> resultDtos = new ArrayList<>();
		ResultTableDTO resultTableDTO = null;
		String trSelectorPath = "table[id=\"%s\"]>tbody>tr";
		trSelectorPath = String.format(trSelectorPath, tbodySelectorElemId);
		String tdSelectorPath = "table[id=\"%s\"]>tbody>tr:nth-of-type(%s)>td:nth-of-type(%s)";
		String tempStatusDate = "";
		
		List<Element> trElements = page.select(trSelectorPath);
		for (int index = 1; index < trElements.size(); index++) {
			tempStatusDate = "";
			Timestamp statusDate = null;
			if(page.select(String.format(tdSelectorPath, tbodySelectorElemId,index,4)).text().equals("")){
				tempStatusDate = page.select(String.format(tdSelectorPath, tbodySelectorElemId,index,3)).text();
				statusDate = AppUtils.convertStringFormatDateToTimeStamp(tempStatusDate,dateFormat);
			} else {
				tempStatusDate = page.select(String.format(tdSelectorPath, tbodySelectorElemId,index,3)).text() + " " + page.select(String.format(tdSelectorPath, tbodySelectorElemId,index,4)).text();
				statusDate = AppUtils.convertStringFormatDateToTimeStamp(tempStatusDate,timestampFormat);
			}
			resultTableDTO = AppUtils.prepareResultTableDtoV2(sourceTableDTO, containerNo,
					page.select(String.format(tdSelectorPath, tbodySelectorElemId,index,1)).text(), statusDate,
					page.select(String.format(tdSelectorPath, tbodySelectorElemId,index,2)).text(), page.select(String.format(tdSelectorPath, tbodySelectorElemId,index,5)).text(),
					page.select(String.format(tdSelectorPath, tbodySelectorElemId,index,6)).text());
			if (resultTableDTO != null) {
				resultDtos.add(resultTableDTO);
			}
		}
		return resultDtos;
	}
	
	public static ResultTableDTO prepareResultTableDtoV2(String carrier, String containerno, String bookingRef,
			String blNumber, String status, Timestamp statusDate, String statusLocation, String statusVessel,
			String statusVoyage, Date minDate) {
		try {
			LOGGER.info("status_date is : {} : minDate is : {}", statusDate, minDate);
			if (minDate != null && statusDate != null) {
				if (statusDate.compareTo(minDate) < 0) {
					return null;
				}
			} else {
				ResultTableDTO resultTableDTO = new ResultTableDTO();
				resultTableDTO.setContainerno(containerno);
				resultTableDTO.setCarrier(carrier);
				resultTableDTO.setBookingRef(bookingRef);
				resultTableDTO.setBlNumber(blNumber);
				resultTableDTO.setStatus(status);
				resultTableDTO.setStatusLocation(statusLocation);
				resultTableDTO.setStatusVessel(statusVessel);
				resultTableDTO.setStatusVoyage(statusVoyage);
				resultTableDTO.setStatusDate(statusDate);
				return resultTableDTO;
			}
		} catch (Exception e) {
			LOGGER.error("Exception while saving status for container: " + containerno, e);
		}
		return null;
	}
}
