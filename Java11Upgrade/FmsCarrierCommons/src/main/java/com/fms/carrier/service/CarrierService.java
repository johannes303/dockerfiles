package com.fms.carrier.service;

import java.sql.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fms.carrier.dao.CarrierDAO;
import com.fms.carrier.dto.ResultTableDTO;
import com.fms.carrier.dto.SourceTableDTO;
import com.fms.carrier.endpoints.ScrapingJobMetricsEndpoint.Carrier;

@Service
public class CarrierService {

	@Autowired
	private CarrierDAO carrierDAO;

	private static final Logger LOGGER = LoggerFactory.getLogger(CarrierService.class);

	public void validateAndSaveResultTableDTO(ResultTableDTO resultTableDTO,Date minDate) {
		try {
			if (resultTableDTO != null) {
				LOGGER.info("status_date is : {} : minDate is : {} " , resultTableDTO.getStatusDate(), minDate);
				if (minDate != null && resultTableDTO.getStatusDate() != null) {
					if (resultTableDTO.getStatusDate().compareTo(minDate) >= 0) {
						carrierDAO.saveContainerStatus(resultTableDTO);
					}
				} else {
					carrierDAO.saveContainerStatus(resultTableDTO);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception while saving status for container: {}", resultTableDTO.getContainerno(), e);
		}
	}
	

	public List<SourceTableDTO> getInputDataset(String carrierName) {
		return carrierDAO.getInputDataset(carrierName);
	}
	
	public void saveResultTableDTO(ResultTableDTO resultTableDTO) {
		carrierDAO.saveContainerStatus(resultTableDTO);
	}

	public void saveResultTableDTOs(List<ResultTableDTO> resultTableDTOs) {
		for (ResultTableDTO resultTableDTO : resultTableDTOs) {
			saveResultTableDTO(resultTableDTO);
		}
	}

	public List<Carrier> getCountOfJobs(String carrier, String status) {
		return carrierDAO.getCountOfJobs(carrier, status);
	}
}