package com.fms.carrier.enums;

public enum CarrierName {

	ACL, MAE, MSC, HLL, ONE, EIM
}
