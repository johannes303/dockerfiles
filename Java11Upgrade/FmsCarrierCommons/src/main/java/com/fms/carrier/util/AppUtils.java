package com.fms.carrier.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fms.carrier.constants.AppConstants;
import com.fms.carrier.dto.QueryTrackerDTO;
import com.fms.carrier.dto.ResultTableDTO;
import com.fms.carrier.dto.SourceTableDTO;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

@Component
public class AppUtils {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(AppUtils.class);
  
  private static WebDriver singletonDriverInstance = null;

  public static Timestamp convertStringToDate(String dateText, String[] dateFormats) {
    int excepCount = 0;
    Date date = null;
    for (String format : dateFormats) {
      try {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
        date = dateFormatter.parse(dateText);
        return new Timestamp(date.getTime());
      } catch (ParseException e) {
        excepCount = excepCount + 1;
      }
    }
    if (excepCount == 2) {
      LOGGER.error("Exception: Date conversion failed for both date formats");
    }
    return null;
  }
  

  public static WebDriver getDriverInstance(String carrierUrl){
    if(singletonDriverInstance == null){
      ChromeOptions options = new ChromeOptions();
      options.addArguments("--headless");
      options.addArguments("--no-sandbox");
      options.addArguments("--proxy-server='direct://'");
      options.addArguments("--proxy-bypass-list=*");
      singletonDriverInstance = new ChromeDriver(options);
      singletonDriverInstance.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    singletonDriverInstance.get(carrierUrl);
        return singletonDriverInstance;
  }
  
  public static Timestamp convertStringFormatDateToTimeStamp(String dateText, String strDateFormat) {
    DateFormat formatter = new SimpleDateFormat(strDateFormat);
    Date convertedDate;
    try {
      convertedDate = formatter.parse(dateText);
      return new Timestamp(convertedDate.getTime());
    } catch (ParseException e) {
      LOGGER.error("Exception occured while date conversion : ",e);
    }
    return null;
  }

  public static ResultTableDTO prepareResultTableDto(String carrier, String containerno, String bookingRef,
      String blNumber, String status, Timestamp statusDate, String statusLocation, String statusVessel,
      String statusVoyage) {
    ResultTableDTO resultTableDTO = new ResultTableDTO();
    resultTableDTO.setContainerno(containerno);
    resultTableDTO.setCarrier(carrier);
    resultTableDTO.setBookingRef(bookingRef);
    resultTableDTO.setBlNumber(blNumber);
    resultTableDTO.setStatus(status);
    resultTableDTO.setStatusLocation(statusLocation);
    resultTableDTO.setStatusVessel(statusVessel);
    resultTableDTO.setStatusVoyage(statusVoyage);
    resultTableDTO.setStatusDate(statusDate);
    return resultTableDTO;
  }
  
  public static ResultTableDTO prepareResultTableDtoV2(SourceTableDTO sourceTableDTO, String containerno, String status,
      Timestamp statusDate, String statusLocation, String statusVessel, String statusVoyage) {
    boolean createDTOFlag = false;
    ResultTableDTO resultTableDTO = null;
    try {
      if (sourceTableDTO.getMinDate() != null && statusDate != null) {
        if (statusDate.compareTo(sourceTableDTO.getMinDate()) < 0) {
          LOGGER.info("status_date :{} minDate : {}",statusDate,sourceTableDTO.getMinDate());
          return null;
        } else {
          createDTOFlag = true;
        }
      } else {
        createDTOFlag = true;
      }
      if (createDTOFlag) {
        resultTableDTO = new ResultTableDTO();
        resultTableDTO.setContainerno(containerno);
        resultTableDTO.setCarrier(sourceTableDTO.getCarrier());
        resultTableDTO.setBookingRef(sourceTableDTO.getBookingRef());
        resultTableDTO.setBlNumber(sourceTableDTO.getBlNumber());
        resultTableDTO.setStatus(status);
        resultTableDTO.setStatusLocation(statusLocation);
        resultTableDTO.setStatusVessel(statusVessel);
        resultTableDTO.setStatusVoyage(statusVoyage);
        resultTableDTO.setStatusDate(statusDate);
      }
    } catch (Exception e) {
      LOGGER.error("Exception while saving status for container: " + containerno, e);
    }
    return resultTableDTO;
  }

  public static long getTimeDiffInMilliSeconds(LocalDateTime lastQueryExectdtime) {
    return ChronoUnit.MILLIS.between(lastQueryExectdtime,LocalDateTime.now());
  }
  
  public static long getTimeDiffInMilliSeconds(LocalDateTime lastQueryExeStartTime,LocalDateTime lastQueryExeEndTime) {
    return ChronoUnit.MILLIS.between(lastQueryExeStartTime,lastQueryExeEndTime);
  }
  
  public static void delay(long sleepIntervalMilliSec) {
    try {
      Thread.sleep(sleepIntervalMilliSec);
    } catch (InterruptedException e) {
      LOGGER.error("Interrupted!", e);
       Thread.currentThread().interrupt();
    }
  }
  
  public static void print(String msg){
    LOGGER.info(msg);
  }

  public static WebDriver webQueryCaller(WebDriver driver, Object obj, QueryTrackerDTO queryTrackerDTO) {
    throttle(queryTrackerDTO);
    if (obj != null) {
      if (obj instanceof By) {
        driver.findElement((By) obj).click();
        queryTrackerDTO.setExecutedQryCountPerMin(queryTrackerDTO.getExecutedQryCountPerMin() + 1);
      } else if (obj instanceof String) {
        driver.get(obj.toString());
        queryTrackerDTO.setExecutedQryCountPerMin(queryTrackerDTO.getExecutedQryCountPerMin() + 1);
      } else if (obj instanceof Actions) {
        ((Actions)obj).click().perform();
      }
      LOGGER.info("WEB***QUERY****CALLER");
    }
    throttle(queryTrackerDTO);
    return driver;
  }
  
  public static SyndFeed webQueryCaller(SyndFeedInput syndFeedInput, XmlReader xmlReader, QueryTrackerDTO queryTrackerDTO) {
    SyndFeed syndFeed = null;   
    throttle(queryTrackerDTO);
    if (syndFeedInput != null) {
      try {
        syndFeed = syndFeedInput.build(xmlReader);
        queryTrackerDTO.setExecutedQryCountPerMin(queryTrackerDTO.getExecutedQryCountPerMin() + 1);
      } catch (IllegalArgumentException | FeedException e) {
        LOGGER.error("Exception occured :", e);
      }
      LOGGER.info("WEB***QUERY****CALLER");
    }
    throttle(queryTrackerDTO);
    return syndFeed;
  }
  
  public static void throttle(QueryTrackerDTO queryTrackerDTO) {
    if (queryTrackerDTO.getLastQueryExeStartTime() == null) {
      queryTrackerDTO.setLastQueryExeStartTime(LocalDateTime.now());
    } else if (queryTrackerDTO.getLastQueryExeEndTime() == null && queryTrackerDTO.getLastQueryExeStartTime() != null) {
      queryTrackerDTO.setLastQueryExeEndTime(LocalDateTime.now());
    }
    
    if (queryTrackerDTO.getLastQueryExeEndTime() != null && queryTrackerDTO.getLastQueryExeStartTime() != null) {
      Double calcSecInterval = Double.valueOf(60d / queryTrackerDTO.getScrapingFrequencyCount());
      calcSecInterval = BigDecimal.valueOf(calcSecInterval).setScale(3, RoundingMode.HALF_UP).doubleValue();
      Double calcMilliSecInterval = calcSecInterval * 1000;
      Double timeTakenForQryExeMilsec = (double) AppUtils.getTimeDiffInMilliSeconds(queryTrackerDTO.getLastQueryExeStartTime(),queryTrackerDTO.getLastQueryExeEndTime());
      if (timeTakenForQryExeMilsec.compareTo(calcMilliSecInterval) < 0) {
        LOGGER.info("Making delay of milli sec : {}" , (calcMilliSecInterval - timeTakenForQryExeMilsec));
        AppUtils.delay((long) (calcMilliSecInterval - timeTakenForQryExeMilsec));
      } else {
        AppUtils.delay(AppConstants.DEFAULT_QUERY_INTERVAL_MLSEC);
      }
    }
  }
  
  public static boolean verifyIfErrorlementExist(WebDriver driver, String tagName, String attrName, String attrValue) {
    boolean returnFlag = false;
    List<WebElement> elements = driver.findElements(By.tagName(tagName)).stream().filter(element -> element.getAttribute(attrName).contains(attrValue))
        .collect(Collectors.toList());
    if (!elements.isEmpty()) {
      returnFlag = true;
    }
    return returnFlag;
  }
  
  public static void testData(List<SourceTableDTO> inputList){
    SourceTableDTO input = new SourceTableDTO();
    input.setCarrier("HLL");
    input.setMinDate(null);
    input.setBookingRef("");
    input.setBlNumber("");
    input.setContainerno("DFSU6047020,HLXU8246461,UACU5597663,UACU5933726");
    inputList.add(input);
  }
  
  public static boolean isNotNullNdEmpty(String str){
    return (str != null && !str.trim().equals("")) ? true : false; 
  }
}
