package com.fms.carrier.service;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.fms.carrier.constants.AppConstants;
import com.fms.carrier.dto.QueryTrackerDTO;
import com.fms.carrier.dto.ResultTableDTO;
import com.fms.carrier.dto.SourceTableDTO;
import com.fms.carrier.util.AppUtils;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

@Service
public class RSSFeedService {

	@Autowired
	private CarrierService carrierService;

	private static final Logger LOGGER = LoggerFactory.getLogger(RSSFeedService.class);

	public ResultTableDTO parseResultMap(Map<String, String> resultsMap) {
		ResultTableDTO resultTableDTO = new ResultTableDTO();
		for (String key : resultsMap.keySet()) {
			String value = resultsMap.get(key).trim();
			if (key.equalsIgnoreCase("Location")) {
				resultTableDTO.setStatusLocation(value);
			} else if (key.equalsIgnoreCase("Description")) {
				resultTableDTO.setStatus(value);
			} else if (key.equalsIgnoreCase("Date")) {
				resultTableDTO.setStatusDate(AppUtils.convertStringFormatDateToTimeStamp(value,
						AppConstants.RSS_FEED_DATE_FORMAT));
			} else if (key.equalsIgnoreCase("Vessel")) {
				resultTableDTO.setStatusVessel(value);
			} else if (key.equalsIgnoreCase("Voyage")) {
				resultTableDTO.setStatusVoyage(value);
			}
		}
		return resultTableDTO;
	}

	public int processRSSFeed(String urlString, SourceTableDTO input, String containerNumber, QueryTrackerDTO queryTrackerDTO) {
		URL feedSource = null;
		SyndFeed feed = null;
		SyndFeedInput feedInput = null;
		List<SyndEntry> syndEntries = new ArrayList<>();
		Map<String, String> resultsMap = new HashMap<>();
		int countOfResults = 0;
		ResultTableDTO resultTableDTO = null;

		try {
			feedSource = new URL(urlString);
			feedInput = new SyndFeedInput();
			feed = AppUtils.webQueryCaller(feedInput, new XmlReader(feedSource),queryTrackerDTO);
			syndEntries = feed.getEntries();
			if (!syndEntries.isEmpty()) {
				for (int i = 0; i < syndEntries.size(); i++) {
					try {
						resultsMap = parseXMLContent(syndEntries.get(i).getDescription().getValue());
						resultTableDTO = parseResultMap(resultsMap);
						if (containerNumber != null) {
							resultTableDTO.setContainerno(containerNumber);
						} else {
							resultTableDTO.setContainerno(input.getContainerno());
						}
						resultTableDTO.setCarrier(input.getCarrier());
						resultTableDTO.setBookingRef(input.getBookingRef());
						resultTableDTO.setBlNumber(input.getBlNumber());
						carrierService.validateAndSaveResultTableDTO(resultTableDTO,input.getMinDate());
					} catch (Exception e) {
						LOGGER.error("Exception occured while preparing/saving resultTableDto: ", e);
					}
				}
				countOfResults = syndEntries.size();
			}
		} catch (MalformedURLException e1) {
			LOGGER.error("Exception occured in processRSSFeed(...) : ", e1);
		} catch (IOException e) {
			LOGGER.error("Exception occured in processRSSFeed(...) : " , e);
		}
		return countOfResults;
	}

	public static Map<String, String> parseXMLContent(String xmlRecords) throws Exception {
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(xmlRecords));
		Document doc = db.parse(is);
		XPath xPath = XPathFactory.newInstance().newXPath();
		String expression = "/table/tr";
		NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
		Map<String, String> resultsMap = new HashMap<>();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element element = (Element) nodeList.item(i);
			NodeList tdNodeList = element.getElementsByTagName("td");
			String value = "";
			if (tdNodeList.item(2).getTextContent().contains(":") && tdNodeList.item(2).getChildNodes().getLength() == 3) {
				value = tdNodeList.item(2).getChildNodes().item(2).getTextContent().split(":")[1];
			} else {
				value = tdNodeList.item(2).getTextContent();
			}
			resultsMap.put(tdNodeList.item(0).getChildNodes().item(0).getTextContent().trim(),value.trim());
		}
		return resultsMap;
	}
}
