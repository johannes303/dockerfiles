package com.fms.carrier.dto;

import java.time.LocalDateTime;

public class QueryTrackerDTO {

	private LocalDateTime lastQueryExeStartTime;

	private LocalDateTime lastQueryExeEndTime;

	private int scrapingFrequencyCount;

	private int executedQryCountPerMin;

	public QueryTrackerDTO() {

	}

	public QueryTrackerDTO(LocalDateTime lastQueryExeStartTime, LocalDateTime lastQueryExeEndTime,
			int scrapingFrequencyCount, int executedQryCountPerMin) {
		super();
		this.lastQueryExeStartTime = lastQueryExeStartTime;
		this.lastQueryExeEndTime = lastQueryExeEndTime;
		this.scrapingFrequencyCount = scrapingFrequencyCount;
		this.executedQryCountPerMin = executedQryCountPerMin;
	}

	public LocalDateTime getLastQueryExeStartTime() {
		return lastQueryExeStartTime;
	}

	public void setLastQueryExeStartTime(LocalDateTime lastQueryExeStartTime) {
		this.lastQueryExeStartTime = lastQueryExeStartTime;
	}

	public LocalDateTime getLastQueryExeEndTime() {
		return lastQueryExeEndTime;
	}

	public void setLastQueryExeEndTime(LocalDateTime lastQueryExeEndTime) {
		this.lastQueryExeEndTime = lastQueryExeEndTime;
	}

	public int getScrapingFrequencyCount() {
		return scrapingFrequencyCount;
	}

	public void setScrapingFrequencyCount(int scrapingFrequencyCount) {
		this.scrapingFrequencyCount = scrapingFrequencyCount;
	}

	public int getExecutedQryCountPerMin() {
		return executedQryCountPerMin;
	}

	public void setExecutedQryCountPerMin(int executedQryCountPerMin) {
		this.executedQryCountPerMin = executedQryCountPerMin;
	}
}