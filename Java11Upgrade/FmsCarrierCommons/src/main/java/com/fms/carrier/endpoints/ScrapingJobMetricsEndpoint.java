package com.fms.carrier.endpoints;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.stereotype.Component;

import com.fms.carrier.service.CarrierService;

@Component
@Endpoint(id = "jobmetrics")
public class ScrapingJobMetricsEndpoint {
	@Autowired
	private CarrierService carrierService;

	List<Carrier> carriers = new ArrayList<>();

	@ReadOperation
	public List<Carrier> getCountOfAllJobs() {
		carriers = carrierService.getCountOfJobs(null, null);
		if (carriers != null && !carriers.isEmpty()) {
			return carriers;
		}
		return new ArrayList<>();
	}

	@ReadOperation
	public List<Carrier> getCountOfAllJobsByStatus(@Selector String status) {
		carriers = carrierService.getCountOfJobs(null, status);
		if (carriers != null && !carriers.isEmpty()) {
			return carriers;
		}
		return new ArrayList<>();
	}

	@ReadOperation
	public List<Carrier> getCountOfJobByStatus(@Selector String carrier, @Selector String status) {
		carriers = carrierService.getCountOfJobs(carrier, status);
		if (carriers != null && !carriers.isEmpty()) {
			return carriers;
		}
		return new ArrayList<>();
	}

	public static class Carrier {
		private String name;
		private String count;

		Carrier() {
		}

		Carrier(String name, String count) {
			this.name = name;
			this.count = count;
		}

		public String getName() {
			return this.name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCount() {
			return count;
		}

		public void setCount(String count) {
			this.count = count;
		}
	}
}
