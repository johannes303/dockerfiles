package com.fms.carrier.constants;

public class AppConstants {
	private AppConstants() {
	}
	public static final String ERROR = "error";
	public static final String OK = "ok";
	public static final String ACL_DATE_FORMAT = "MM/dd/yy HH:mm";
	public static final String MSC_DATE_FORMAT = "dd/MM/yyyy";
	public static final String MAE_DATE_FORMAT1 = "dd MMM yyyy HH:mm";
	public static final String MAE_DATE_FORMAT2 = "dd MMM yyyy";
	public static final String RSS_FEED_DATE_FORMAT = "MMMM dd, yyyy";
	public static final long DEFAULT_QUERY_INTERVAL_MLSEC = 200;
	public static final long TIMER_DURATION_IN_SECS = 60;
}
